"""Singly Linked List"""


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class SLL:
    def __init__(self):
        self.head = None

    def display(self):
        current_node = self.head
        while current_node is not None:
            print(f"{current_node.data}", end="->")
            current_node = current_node.next

    def append(self, data):
        if self.head is None:
            self.head = Node(data)
        else:
            current_node = self.head
            while current_node.next is not None:
                current_node = current_node.next
            current_node.next = Node(data)

    def insert(self, data, index):
        if self.head == None:
            self.head = Node(data)
        else:
            current_node = self.head
            i = 0
            while current_node.next is not None:
                if i == index-1:
                    break
                current_node = current_node.next
                i += 1
            new_node = Node(data)
            new_node.next = current_node.next
            current_node.next = new_node

    def remove(self, index):
        previous_node = self.head
        current_node = previous_node.next
        i=0
        while current_node.next is not None:
            if i == index-1:
                break
            previous_node = current_node.next
            current_node = previous_node.next
            i+=1
        previous_node.next = current_node.next

    def length(self):
        current_node = self.head
        i=0
        while current_node.next is not None:
            i+=1
            current_node = current_node.next
        return i
