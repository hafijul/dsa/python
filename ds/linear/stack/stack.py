class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class Stack:
    top = None
    def push(self, data):
        
        if Stack.top is None:
            node = Node(data)
            Stack.top = node

        else:
            newNode = Node(data)
            newNode.next = Stack.top
            Stack.top = newNode

    def pop(self):
        top = Stack.top
        if top is None:
            print("Stack Underflow")
            return
        print(f"{top.data} popped out of Stack")
        Stack.top = top.next
        top.next = None

    def peek(self):
        if Stack.top is None:
            print("Stack Empty")
            return
        else:
            print(Stack.top.data)
    def print(self):
        current = Stack.top
        while(current is not None):
            print(f"|{current.data: ^10}|")
            current = current.next
        

if __name__ == "__main__":
    stack = Stack()
    command = "1"
    while(command != "0"):
        command=input('''Enter command
        1)Push
        2)Pop
        3)Peek
        4)PRINT
        5)is Full
        6)is Empty
        0)EXIT
        ''')
        if command == "1":
            T=int(input("Enter number of items to push : "))
            for i in range(T):
                data = input("Enter a value to push into stack :")
                stack.push(data)
        elif command == "2":
            stack.pop()
        elif command == "3":
            stack.peek()
        elif command == "4":
            stack.print()
        elif command == "5":
            stack.peek()
                
        

